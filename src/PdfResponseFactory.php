<?php

class PdfResponseFactory
{

	/** @var string */
	private $tempDir;

	public function __construct($tempDir)
	{
		$this->tempDir = $tempDir;
	}

	public function create()
	{
		$response = new PdfResponse();
		$response->setTempDir($this->tempDir);

		return $response;
	}

}
